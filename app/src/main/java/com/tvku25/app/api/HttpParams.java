package com.tvku25.app.api;

public class HttpParams {
	//Url
	public static final String BASE_URL = "https://script.google.com/macros/s/AKfycbzj_b_lyrqhynJFe7vfS1kXsugz6Lsfc7V6GuqL4OWkxsSprLu7/";
	public static final String SHEET_API_END_POINT = "exec?"; // sheet api end point

	// Sheet Information
	public static final String SHEET_ID = "1PQBUGCPKhvtVJLth_V9-DleIQ8gS5oJfQvJ2QBKo8nA"; // Replace by your sheet id
	// Replace by your sheet name
	public static final String SHEET_NAME = "channel";
	public static final String SHEET_NAME_CATEGORY = "category";
}
