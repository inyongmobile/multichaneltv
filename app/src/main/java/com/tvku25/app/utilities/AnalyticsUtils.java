package com.tvku25.app.utilities;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public class AnalyticsUtils {
	private static AnalyticsUtils analyticsUtils;
	private FirebaseAnalytics mFireBaseAnalytics;

	private AnalyticsUtils(Context context) {
		mFireBaseAnalytics = FirebaseAnalytics.getInstance(context);
	}

	public static AnalyticsUtils getAnalyticsUtils(Context context) {
		if (analyticsUtils == null) {
			analyticsUtils = new AnalyticsUtils(context);
		}
		return analyticsUtils;
	}

	public void trackEvent(String eventName) {
		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, eventName);
		mFireBaseAnalytics.logEvent("PAGE_VISIT", bundle);
	}

}
