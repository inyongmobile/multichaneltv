package com.tvku25.app.listener;

import android.view.View;

public interface OnItemClickListener {
	void onItemListener(View view, int position);
}
