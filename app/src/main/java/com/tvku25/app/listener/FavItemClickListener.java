package com.tvku25.app.listener;

import android.view.View;

public interface FavItemClickListener {
	void onItemListener(View view, int position);

	void onFavIconListener(View view, int position);
}
